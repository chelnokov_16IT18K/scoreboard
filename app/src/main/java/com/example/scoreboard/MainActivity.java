package com.example.scoreboard;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;

/**
 * Класс экрана приложения "Футбольное табло"
 */
public class MainActivity extends AppCompatActivity {
    private int counterFrance = 0;//счётчики как у кликера, но для каждой страны
    private int counterGerman = 0;
    private final String FRANCE_LOGO = "\uD83C\uDDEB\uD83C\uDDF7 \n";//символы Юникод(АХАХАХА А ЧТО ИМЭЙДЖ ВЬЮ ЧТО ЛИ???)
    private final String GERMAN_LOGO = "\uD83C\uDDE9\uD83C\uDDEA \n";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        createButton(R.id.btngerman, GERMAN_LOGO + getResources().getString(R.string.german) + "\n", counterGerman);
        //лол ну тут гетстринг просто нельзя вынести из методов, ч бы сделал всё константно, просто по-другому локализация не выходит(((
        createButton(R.id.btnfrance, FRANCE_LOGO + getResources().getString(R.string.france) + "\n", counterFrance);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                counterFrance = 0;
                counterGerman = 0;
                createButton(R.id.btngerman, GERMAN_LOGO + getResources().getString(R.string.german) + "\n", counterGerman);
                createButton(R.id.btnfrance, FRANCE_LOGO + getResources().getString(R.string.france) + "\n", counterFrance);
            }
        });
    }

    /**
     * прибавляет счётчик страны при нажатии на кнопку
     * @param view нажатая кнопка Франции
     */
    public void onClickFrance(View view) {
        counterFrance++;
        createButton(R.id.btnfrance, FRANCE_LOGO + getResources().getString(R.string.france) + "\n", counterFrance);
    }

    /**
     * то же самое
     * @param view Германии, соответственно
     */
    public void onClickGerman(View view) {
        counterGerman++;
        createButton(R.id.btngerman, GERMAN_LOGO + getResources().getString(R.string.german) + "\n", counterGerman);
    }

    /**
     * сохраняет и передаёт значения счётчиков стран
     * @param outState объект для передачи значений
     */
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putInt("france", counterFrance);
        outState.putInt("german", counterGerman);
        super.onSaveInstanceState(outState);
    }

    /**
     * принимает сохранённые значения и восстанавливает их
     * @param savedInstanceState объект со значениями
     */
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        counterFrance = savedInstanceState.getInt("france");
        counterGerman = savedInstanceState.getInt("german");
        super.onRestoreInstanceState(savedInstanceState);

        createButton(R.id.btngerman, GERMAN_LOGO + getResources().getString(R.string.german) + "\n", counterGerman);
        createButton(R.id.btnfrance, FRANCE_LOGO + getResources().getString(R.string.france) + "\n", counterFrance);
    }

    /**
     * создаёт и отображает кнопку
     * @param p - id кнопки в layout
     * @param country название страны (текст в кнопке)
     * @param counter значение счётчика на кнопке
     */
    private void createButton(int p, String country, int counter) {
        Button button = findViewById(p);
        String teamName = country + counter;
        button.setText(teamName);
    }

}
